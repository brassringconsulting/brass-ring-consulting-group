Brass Ring Consulting Group is a full-service consulting firm founded by seasoned professionals and entrepreneurs.? We've been running our own business operations and making our clients successful for over 25 years. Our experience gives us the ability work with your business "holistically."

Address: 515 South Flower Street, 18th Floor, Suite 1847, Los Angeles, CA 90071, USA

Phone: 323-850-1812